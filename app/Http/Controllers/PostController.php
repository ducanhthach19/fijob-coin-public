<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Post::orderBy('created_at', 'DESC')->paginate(6);
    }

    public function search(Request $request)
    {
        $query = $request->get('query');
        $list = Post::where('title', 'like', "%{$query}%")
            ->orWhere('excerpt', 'like', "%{$query}%")
            ->orWhere('slug', 'like', "%{$query}%")
            ->paginate(6);
        if (empty($list[0])) {
            $list = Post::orderBy('created_at', 'DESC')->paginate(6);
            return response()->json(["status" => false, "data" =>  $list], 200);
        }
        return response()->json(["status" => true, "data" =>  $list], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $list = new \stdClass();
        $list->category = Category::find($post->category_id)->slug;
        $list->author_id = $post->author_id;
        $list->created_at = $post->created_at;
        $list->body = $post->body;
        $list->excerpt = $post->excerpt;
        $list->image = $post->image;
        $list->id = $post->id;
        $list->slug = $post->slug;
        $list->status = $post->status;
        $list->title = $post->title;
        $list->featured = $post->featured;
        return $list;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
